//Sarah Loher
//November 26th, 2018
//This homework gives you practice with arrays and in searching single dimensional arrays
//Next, prompt the user to enter a grade to be searched for. Use binary search to find the entered grade. 
//Indicate if the grade was found or not, and print out the number of iterations used. 
//This program will have seperate methods for the linear search, binary search and random scrambling

import java.util.Scanner; 
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class CSE2Linear{
  public static void main(String args[]){
    Scanner sc = new Scanner(System.in);//This creates a scanner
    int[] list = new int[15];//This creates an array that will hold the grades
    for (int i = 0; i < list.length; i++){//This places the user input into the different spots in the array
        System.out.println("Please enter a final grade:");//This prompts the user to enter a grade one at a time
        list[i] = sc.nextInt();//This stores the inputted grade
    }
    System.out.println("List of grades" + Arrays.toString(list));//This prints the list of what the user put in
    System.out.println("Enter a grade to be searched for:");//This asks for what grade they want to find the index of
    int grade = sc.nextInt();//This stores the grade to be searched for
    
    System.out.println(binary(list, grade));//This prints the result of the binary search that uses the grades entered and the grade to be searched for
    scramble(list);//This scrambles the list of grades and prints it
    System.out.println("Enter another grade to be searched for:");//This asks for another grade to be searched for, this time using linear search
    int linearGrade = sc.nextInt();//This stores the value prompted for above
    
    System.out.println(linear(list, linearGrade));//This prints the results of the linear search
  }
  public static String binary(int[] list, int grade){//This method conducts the binary search through the original list
      int mid;
      int low;
      int high;
      int times = 1;//This sets the counter for how many iterations are used at one because it always looks at least 1 time
     
      low = 0;//This sets the low at an index of 0, which is the first grade
      high = list.length - 1;//This sets the high at the max number which is the length minus 1, this is the last grade entered

      while (high >= low) {//This loop runs until it finds the number
         mid = (high + low) / 2;//This sets the mid at the middle between the highest and 0
         if (list[mid] < grade) {//This finds if the middle is less than the grade and searches the top half of the array
            low = mid + 1;//It will search in the top half because the new low is one above the middle grade
            times++;//It increments the counter every time it checks
         } 
         else if (list[mid] > grade) {//This will run if the middle is greater than the grade and searches the lower half of the array
            high = mid - 1;//This sets the new high to one less than the previous mid to only search the bottom half
            times++;//The counter is incremented every time it runs
         } 
         else {//This will run when the value is found
            return "The value was found at index: " + mid +" using " + times + " iterations";//This will return a message with the index of the grade
         }
      }

      return "The grade was not found"; //This will return if the grade was not in the index
   }
  public static void scramble(int[] list){//This method will scramble up the array
    int[] randomString = new int[15];//This creates a new int array with enough space for all the grades
    String printingRandom = "";//This is an empty string to hold what gets printed
    Random rnd = ThreadLocalRandom.current();//This creates a random thread
      for (int l = list.length - 1; l > 0; l--){
        int index = rnd.nextInt(l + 1);//This changes index
        int rand = list[index];//This creates a int called rand
        list[index] = list[l];//this sets the two arrays equal
        list[l] = rand;
        randomString = list;//this makes random string equal to the original string
        printingRandom = Arrays.toString(randomString);//This changes the array to a printable string
  }
  System.out.println("The shuffled list of grades is:" + printingRandom);//This prints the shuffled list
  }
  public static String linear(int[] list, int linearGrade){//This method will conduct a linear search
    int k;
    int count = 1;//This sets the counter for iterations at 1 because it always looks one time
      for (k = 0; k < list.length; ++k) {
         if (list[k] == linearGrade) {//This will find if the first member and so on are equal to the grade to be searched for
            return "The grade was found at index:" + k + " using " + count + " iterations";//If it is equal it returns this
         }
         else{
           count++;//If it is not equal it increments the counter and tries again
         }
      }     
      return "The grade was not found"; //This will return if the grade is not found
  }
}