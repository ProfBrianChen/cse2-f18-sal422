//Sarah Loher
//November 26th, 2018

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class RemoveElements{
  public static void main( String args[]){
    Scanner sc = new Scanner(System.in);//This imports the scanner class
    
    int num[] = new int[10];//This creates an array with 10 spots
    int array1[];
    int array2[];
    int index;
    int target;
    String answer = "";
    
    do{//This first block will run regardless and then the while will determine whether it runs again
      System.out.println("Randomly input 10 ints [0-9]");//This asks the user for 10 ints
      num = randomInput();
      String out = "The original array is: ";//This is the statement before the original array
      out += listArray(num);//This combines the statement and the array
      System.out.println(out);//This prints it all out
      
      System.out.print("Enter the index: ");//This asks for what index the user wants to remove
      index = sc.nextInt();//This stores that value
      
      while((index<0)||(index>num.length-1)){//This runs while the index is less than 0 or the index is greater than the max index
        int junk = sc.nextInt();
        System.out.println("Integer must be between 0 and 9");//This tells the user it is out of bounds
        index = sc.nextInt();//This collects a new index
      }
      array1 = delete(num,index);//This sets array1 equal to the return value of delete when num and index are used
      System.out.println("Index " + index + " has been removed");//This prints what was removed
      String out1 = "The output array is: ";
      out1 += listArray(array1);//This combines the statement above and array1
      System.out.println(out1);//This prints them out
      
      System.out.println("Enter the target value: ");//This asks the user for a target value to remove
      target = sc.nextInt();//This stores that value
      array2 = remove(num, target);//This sets array2 equal to the return of the remove method using num and target
      String out2 = "The output array is: ";
      System.out.println(out2 + Arrays.toString(array2));//This prints out the statement and array2
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit: ");//This prompts the user to enter whether they want to try again or not
        answer = sc.next();//This stores their value for use in the while loop below
    }
    while(answer.equals("Y")||answer.equals("y"));//This runs the entire loop again if the user enter either Y or y
  }
  public static String listArray(int num[]){//This method will print out the array
    String out = "";//This creates an empty string which will be the output
    for(int j=0;j<num.length;j++){
  	  if(j>0){
    	out+=", ";
  	  }
  	  out+=num[j];//This adds the the list
	  }
	return out;//This returns the list
  }
  public static int[] randomInput(){//This method gives random output
    int[] random = new int[10];//This creates a new array with 10 members
    for(int i = 0; i<random.length; i++){//This loop will run and fill the array with random numbers
      random[i] = (int)(Math.random()*10+1);//This sets each index to different numbers
    }
    System.out.println(Arrays.toString(random));//This prints the random array
    return random;//THis returns the random array
  }
  public static int[] delete(int[] list, int pos){//This method will delete the number at a given index
    if (pos<0 || pos>=list.length){//This will run if the position is out of bounds
      return list;//It will return the original list
    }
    int[] second = new int[list.length-1];//THis creates a new array with one less member
    for(int i=0, j=0; i<list.length; i++){
      if(i==pos){//If i is equal to the position it will continue
        continue;
      }
      second[j++]= list[i];//If the for loop is true this will happen and it will change the array
    }
    return second;//This will return the new array
  }
  public static int[] remove(int[] list, int target){//This method will remove a specific number
    for(int s: list){
      if(s==target){//If the temporary variable is equal to the target from the user this will run
        System.out.println("Element " + target + " has been found");//This will print with if it was found
        break;//It will skip the rest of the loop
      }
      else{//If they are not equal then this will run that it was not found
        System.out.println("Element " + target + " was not found");
      }
      break;//THis will end the loop
    }
    int occurences = 0;//This sets the counter at 0 for how many times it shows up
    for(int i=0; i<list.length; i++){
      if(list[i] != target){//if the value is not equal to the target this runs
        list[occurences++] = list[i];
      }
    }
    return Arrays.copyOf(list, occurences);//This returns a copied statement 
  }
}