//Sarah Loher
//September 13th, 2018
//This program aims to help users to split a check for dinner evenly
//It will take the original cost of the check and the percentage of tip they wish to pay
//It will then ask how many ways they wish to split it
//Finally, it will split the check for the users

import java.util.Scanner;

public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in );

  System.out.print("Enter the original cost of the check in the form xx.xx: ");  
          //This will ask the user for the cost of the meal
  double checkCost = myScanner.nextDouble();
          //This will allow them to enter the cost

  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):" );
          //This will ask how much tip they want to pay
  double tipPercent = myScanner.nextDouble();
          //This will allow them to input the value
  tipPercent /= 100; 
          //We want to convert the percentage into a decimal value

  System.out.print("Enter the number of people who went out to dinner: ");
          //This will ask the user how many people will be splitting the bill
  int numPeople = myScanner.nextInt();
          //This will allow them to enter the integer

  double totalCost;
          //This declares the total cost as a variable
  double costPerPerson;
          //This declares the cost PER PERSON as a variable
  int dollars;   
          //Whole dollar amount of cost 
  int dimes;
  int pennies; 
          //The two above are used for storing digits to the right of the decimal point for the cost$ 
  totalCost = checkCost * (1 + tipPercent);
          //This will find the total cost which is the cost of the meal plus the tip multiplied and added
  costPerPerson = totalCost / numPeople;
         //This will get the whole amount, dropping decimal fraction
  dollars =(int)costPerPerson;
       //This will get the dollars
  dimes=(int)(costPerPerson * 10) % 10;
          //This will find the dimes place
  pennies=(int)(costPerPerson * 100) % 10;
          //This will find the pennies place
  System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
          //This will print out the total cost that each person owes
}  //end of main method   
  	} //end of class

