//Sarah Loher
//September 6, 2018
//CSE02 Lab 02
// Program will print the number of minutes for each trip
//It will print the number of counts for each trip
//It will print the distance for each trip in miles
//It will print the distance for the two trips combined

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

	   	int secsTrip1=480;  //This is the number of seconds in the first trip
      int secsTrip2=3220;  //This is the number of seconds in the second trip
		  int countsTrip1=1561;  //This is the number of bike rotations in the first trip
		  int countsTrip2=9037; //This is the number of bike rotations in the second trip

      double wheelDiameter=27.0,  //This is the diameter of the wheel
  	  PI=3.14159, //This is the value for Pi
  	  feetPerMile=5280,  //This is the amount of feet in 1 mile
  	  inchesPerFoot=12,   //This is the amount of inches in 1 foot
  	  secondsPerMinute=60;  //This is the amount of seconds in a minute
      double distanceTrip1, distanceTrip2,totalDistance;  //These are the variables for the distances of each trip
      
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

  distanceTrip1=countsTrip1*wheelDiameter*PI;//This gives the distance of Trip1 in inches
    	// For each count, a rotation of the wheel travels the diameter in inches times Pi
	distanceTrip1/=inchesPerFoot*feetPerMile; //This gives the distance of trip1 in miles while line 32 gives it in inches times pi
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//This gives the distance of Trip2
	totalDistance=distanceTrip1+distanceTrip2;//This gives the total distance which is the two trips added

      //The following lines print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");//This prints the distance of trip1 in a sentence
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");//This prints the distance of trip2 in a sentence
	    System.out.println("The total distance was "+totalDistance+" miles");//This prints the total distance in a sentence

	}  //end of main method   
} //end of class
