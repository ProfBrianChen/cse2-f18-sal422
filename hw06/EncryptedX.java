//Sarah Loher
//October 22, 2018
//This program will ask the user for how big they want the box
//It will check if the input is an int and if it is not it will ask again for an int
//It then uses the user value to create a box with spaces to encrypt the X

import java.util.Scanner;//This is the scanner class

public class EncryptedX{
  public static void main(String args[]){//This is the main method
  Scanner sc = new Scanner(System.in);//This is the scanner class
    System.out.println("Enter an integer between 1-100");//This displays the message asking for an int
  while(!sc.hasNextInt()){//This finds if the input is NOT true
    System.out.println("You didn't enter an integer. Please try again.");//This will print if the input from the scanner is not an int
      sc.next();//This will ask the user again
  }
  int s = sc.nextInt();//This stores the value that the user inputs when it is correctly an int
    for (int i = 0; i < s+1; ++i) {
      for (int j = 0; j < s+1; ++j) {
        if (i == j) {//This evalutes whether the current location is a location of a space or a *
            System.out.print(" ");//This will print if it is a space
        } else if (i == s - j ) {
            System.out.print(" ");//This will print the other half if a space belongs there
        } else {
            System.out.print("*");//This will print a star otherwise
        }

    }
    System.out.println();//This will return to the next line
      }
  
}
  
}

  