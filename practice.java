//Sarah Loher
//October 25th, 2018
//This program will use the random class to generate random parts of a story and then put them together
//This will use multiple methods

import java.util.Random;
import java.util.Scanner;

public class Storybook{
  public static void main(String args[]){
      Random randomGenerator = new Random();
      Scanner sc = new Scanner(System.in);
    System.out.println("Would you like to generate a sentence? Enter 0 for yes and 1 for no.");
      int yesOrNo = sc.nextInt();
    int randomIntAdj = randomGenerator.nextInt(10);
    int randomIntSub = randomGenerator.nextInt(10);
    int randomIntVerb = randomGenerator.nextInt(10);
    int randomIntObj = randomGenerator.nextInt(10);
     String adjective = adj(randomIntAdj);
     String sub = subject(randomIntSub);
     String verb = pastVerb(randomIntVerb);
     String obj = object(randomIntObj);
    System.out.println("A " + adjective+ " " + sub+ " " + verb+ " with a " + obj);
    int generateRandom;int lastRandomNumber;
   
    int randomNumber = random.nextInt(UPPER_BOUND - 1) + 1;
    if(randomNumber == lastRandomNumber){
        randomNumber = 0;
    }
    return randomNumber;
    randomNumber = randomIntAdj;
    }
  public static String adj(int randomIntAdj){
    Random randomGenerator = new Random();
   // int randomIntAdj = randomGenerator.nextInt(10);
    String adjective = "";
    switch(randomIntAdj){
      case 0: adjective = "friendly";
        break;
      case 1: adjective = "strange";
        break;
      case 2: adjective = "happy";
        break;
      case 3: adjective = "funny";
        break;
      case 4: adjective = "smart";
        break;
      case 5: adjective = "brave";
        break;
      case 6: adjective = "new";
        break;
      case 7: adjective = "young";
        break;
      case 8: adjective = "old";
        break;
      case 9: adjective = "important";
        break;     
    }
   return adjective;
  }
  public static String subject(int randomIntSub){
     Random randomGenerator = new Random();
   // int randomIntSub = randomGenerator.nextInt(10);
    String sub = "";
    switch(randomIntSub){
      case 0: sub = "dog";
        break;
      case 1: sub = "cat";
        break;
      case 2: sub = "hippo";
        break;
      case 3: sub = "fish";
        break;
      case 4: sub = "human";
        break;
      case 5: sub = "alien";
        break;
      case 6: sub = "monkey";
        break;
      case 7: sub = "dinosaur";
        break;
      case 8: sub = "adult";
        break;
      case 9: sub = "child";
        break;     
    }
    return sub;
  }
  public static String pastVerb(int randomIntVerb){
     Random randomGenerator = new Random();
   //int randomIntVerb = randomGenerator.nextInt(10);
    String verb = "";
    switch(randomIntVerb){
      case 0: verb = "ran";
        break;
      case 1: verb = "sat";
        break;
      case 2: verb = "swam with";
        break;
      case 3: verb = "ate";
        break;
      case 4: verb = "drove";
        break;
      case 5: verb = "fed";
        break;
      case 6: verb = "admired";
        break;
      case 7: verb = "asked";
        break;
      case 8: verb = "added";
        break;
      case 9: verb = "asked";
        break;     
    }
    return verb;
  }
  public static String object(int randomIntObj){
     Random randomGenerator = new Random();
   //int randomIntObj = randomGenerator.nextInt(10);
    String obj = "";
    switch(randomIntObj){
      case 0: obj = "cat";
        break;
      case 1: obj = "dog";
        break;
      case 2: obj = "bird";
        break;
      case 3: obj = "monkey";
        break;
      case 4: obj = "friend";
        break;
      case 5: obj = "kid";
        break;
      case 6: obj = "sister";
        break;
      case 7: obj = "brother";
        break;
      case 8: obj = "mom";
        break;
      case 9: obj = "dad";
        break;     
    }
    return obj;
  }
  }
