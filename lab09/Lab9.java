//Sarah Loher
//November 15, 2018
import java.util.Arrays;

public class Lab9{
  public static void main(String args[]){
    int[] input = {1,2,3,4,5,6,7,8,9};
    System.out.println("Initial array:" + Arrays.toString(copy(input)));
    inverter(input);
    int[] sample = {2,4,6,7,8,9,5,3,5,74};
    print(sample);
    print(inverter2(input));
  }
  public static int[] copy(int[] input){
    //int[] array0 = new int[9];
    int[] copied = new int[input.length];
    for(int i=0; i<input.length; i++){
      copied[i]=input[i];
    }
    return copied;
  }
  public static void inverter(int[] input){
    int temp;
    for (int i = 0; i < input.length/2; i++){
    temp = input[i];
    input[i] = input[input.length-1-i];
    input[input.length-1-i] = temp;
    }
    System.out.println("Array After Reverse : "+ Arrays.toString(input));
    }
  public static int[] inverter2(int[] input){
    int[] copy2 = new int[input.length];
    copy2 = copy(input);
    int[] inverted2 = new int[input.length];
    inverter(input);
    return input;
  }
  public static void print(int[] input){
    System.out.println(Arrays.toString(input));
   
  }
}