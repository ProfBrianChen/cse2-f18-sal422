//Sarah Loher
//This program will preform an selection sort on two arrays

import java.util.Arrays;
public class SelectionSort {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}

	/** The method for sorting the numbers */
	public static int selectionSort(int[] numbers) { 
		System.out.println(Arrays.toString(numbers));//prints array to start
		int iterations = 0;//counter at 0
		  int i;
      int j;
      int indexSmallest;
      int temp;      // Temporary variable for swap

      for (i = 0; i < numbers.length-1; ++i) {
         iterations++;
         indexSmallest = i;
         for (j = i + 1; j < numbers.length; ++j) {
           iterations++;
            if (numbers[j] < numbers[indexSmallest]) {
               indexSmallest = j;
               System.out.println(Arrays.toString(numbers));
            }
         }
         temp = numbers[i];
         numbers[i] = numbers[indexSmallest];
         numbers[indexSmallest] = temp;
      }
    return iterations;
   }
		
	}
