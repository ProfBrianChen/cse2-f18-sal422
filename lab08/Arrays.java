//Create 2 array of integers with each having a size of 100.    
//2)    Fill in one of the arrays with randomized integers in the range of 0 to 99, using math.random().
//3)  Use the second array to hold the number of occurrences of each number in the first array. 
//4)  See sample run and output below. Note for the sample run I am using less than 100 numbers 

public class Arrays{
  public static void main(String args[]){
    int [] numbers = new int[100];
    int [] occurences = new int[100];
    int i;
    int temp = 0;
    for(i = 0; i<numbers.length; i++){
      numbers[i] = (int)(Math.random()*100);
      temp = numbers[i];
      occurences[temp]= occurences[temp]+1;
    }
    System.out.println(temp+ " occurs "+ occurences[temp] + " times");
  }
}