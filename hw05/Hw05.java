//Sarah Loher
//October 8th, 2018
//This program will ask the user how many times it should generate hands. 
//For each hand, the program will generate five random numbers from 1 to 52, 
//where these 52 cards represent the four sets of 13 cards associated with each suit.  
//This program should check if this set of five cards belongs to one of the four hands listed in the above. 
//If so, the counter for this hand should be incremented by one. 
//After all hands have been generated, the program will calculate the probabilities of each of the four hands described above 
//by computing the number of occurrences divided by the total number of hands

import java.util.Scanner;
public class Hw05{
  public static void main(String[] args){
    String none;
    
    Scanner scan = new Scanner(System.in);
    System.out.println("How many hands would you like to generate (enter and integer)");
    boolean number = scan.hasNextInt();//This checks if the input is an int
    
    int num = scan.nextInt();
    
    int counter = 0;
    
    double probFourOfSame;
      
    while(number==false)
    {
      none = scan.next(); 
      System.out.println("How many hands would you like the generate (enter an integer) "); //This asks the question again
      number = scan.hasNextInt();
    }
    for(int x=0; x<num; x++)
    {
      int cardOne = (int) (Math.random()* 52+1);
      int cardTwo = (int) (Math.random()* 52+1);
      int cardThree = (int) (Math.random()* 52+1);
      int cardFour = (int) (Math.random()* 52+1);
      int cardFive = (int)(Math.random()* 52+1);
      
      if( (cardOne==1) || (cardOne==14) || (cardOne==27)|| (cardOne==40)){
        if( (cardTwo==1)||(cardTwo==14)||(cardTwo==27)||(cardTwo==40)){
          if((cardThree==1)||(cardThree==14)||(cardThree==27)||(cardThree==40)){
            if((cardFour==1)||(cardFour==14)||(cardFour==27)||(cardFour==40)){
              if((cardFive==1)||(cardFive==14)||(cardFive==27)||cardFive==40){
                counter++;
              }
            }
          }
        }      
      }
      if( (cardOne==2) || (cardOne==15) || (cardOne==28)|| (cardOne==41)){
        if( (cardTwo==2)||(cardTwo==15)||(cardTwo==28)||(cardTwo==41)){
          if((cardThree==2)||(cardThree==15)||(cardThree==28)||(cardThree==41)){
            if((cardFour==2)||(cardFour==15)||(cardFour==28)||(cardFour==41)){
              if((cardFive==2)||(cardFive==15)||(cardFive==28)||cardFive==41){
                counter++;
              }
            }
          }
        }   
      }
      if( (cardOne==3) || (cardOne==16) || (cardOne==29)|| (cardOne==42)){
        if( (cardTwo==3)||(cardTwo==16)||(cardTwo==29)||(cardTwo==42)){
          if((cardThree==3)||(cardThree==16)||(cardThree==29)||(cardThree==42)){
            if((cardFour==3)||(cardFour==16)||(cardFour==29)||(cardFour==42)){
              if((cardFive==3)||(cardFive==16)||(cardFive==29)||cardFive==42){
                counter++;
              }
            }
          }
        }      
      }
      if( (cardOne==4) || (cardOne==17) || (cardOne==30)|| (cardOne==43)){
        if( (cardTwo==4)||(cardTwo==17)||(cardTwo==30)||(cardTwo==43)){
          if((cardThree==4)||(cardThree==17)||(cardThree==30)||(cardThree==43)){
            if((cardFour==4)||(cardFour==17)||(cardFour==30)||(cardFour==43)){
              if((cardFive==4)||(cardFive==17)||(cardFive==30)||cardFive==43){
                counter++;
              }
            }
          }
        }      
      }
       if( (cardOne==5) || (cardOne==18) || (cardOne==31)|| (cardOne==44)){
        if( (cardTwo==5)||(cardTwo==18)||(cardTwo==31)||(cardTwo==44)){
          if((cardThree==5)||(cardThree==18)||(cardThree==31)||(cardThree==44)){
            if((cardFour==5)||(cardFour==18)||(cardFour==31)||(cardFour==44)){
              if((cardFive==5)||(cardFive==18)||(cardFive==31)||cardFive==44){
                counter++;
              }
            }
          }
        }      
      }
        System.out.println("You rolled " + cardOne + cardTwo + cardThree + cardFour + cardFive);
      
      }
      System.out.println("Number of loops: " + num);
      probFourOfSame = (counter)/num;
      System.out.println(probFourOfSame);
    }
  }
