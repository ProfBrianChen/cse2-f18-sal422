//Sarah Loher
//October 3rd, 2018
//This program will ask the user about one of the courses they are taking

import java.util.Scanner;
public class Loops{
    			// main method required for every Java program
   			public static void main(String[] args) {
        
          Scanner myScanner = new Scanner( System.in );
          System.out.println("What is the course number of your course?");
          boolean courseNumber = myScanner.hasNextInt();
          System.out.println("What is the name of the department that your course is in?");
          boolean departmentName = myScanner.hasNextLine();
          System.out.println("How many times per week is your course?");
          boolean timesPerWeek = myScanner.hasNextInt();
          System.out.println("What is the start time of your course?");
          boolean startTime = myScanner.hasNextDouble();
          System.out.println("What is your intructors last name?");
          boolean instructorName = myScanner.hasNextLine();
          System.out.println("How many students are in your class?");
          boolean numberOfStudents = myScanner.hasNextInt();
          
          if(courseNumber){
            int courseNumber1 = myScanner.nextInt();
          }
          else{
            myScanner.next();
            System.out.println("Invalid course number please enter again");
          }
          if(departmentName){
            String departmentName1 = myScanner.nextLine();
          }
          else{
            myScanner.next();
            System.out.println("Invalid department name please enter again");
          }
          if(timesPerWeek){
            int timesPerWeek1 = myScanner.nextInt();
          }
          else{
            myScanner.next();
            System.out.println("Invalid times per week please enter again");
          }
        }
}
