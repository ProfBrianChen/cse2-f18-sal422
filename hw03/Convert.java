//Sarah Loher
//September 15, 2018
//This program aims to collect values from a user about the amount of rainfall in an area due to a hurricane
//It will ask for the area in acres and the inches of rainfall and then convert these to cubic miles
//It will then print the result to the console
import java.util.Scanner;
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
        
        Scanner myScanner = new Scanner( System.in );
        //This is what allows the user to input numbers when prompted
        System.out.print("Enter the affected area in acres:");
        double areaAcres = myScanner.nextDouble();
        //This asks the user for the affected area and stores their value
        System.out.print("Enter the rainfall in the affected area:");
        double areaRainfall = myScanner.nextDouble();
        //This asks the user for the amount of rainfall and stores their value
        double acreInch = (areaAcres * areaRainfall);
        //This does the math operation to find the acre-inch
        double cubicMileConversion = 2.4660669E-8;
        //This is the conversion factor between acre inches and cubic miles that I found in the online converters
        double cubicMile = (acreInch * cubicMileConversion);
        //This converts the acre-inch to cubic miles
        System.out.println(cubicMile + " cubic miles");
        //This prints the value in cubic miles
}  //end of main method   
  	} //end of class
