//Sarah Loher
//September 15, 2018
//This program aims to collect values for the dimensions of a pyramid from a user
//It will use these values to calculate the volume inside the pyramid
//It will then print the result to the console
import java.util.Scanner;
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in );
          //This is the scanner function that will allow the user to input their own values
          System.out.print("The square side of the pyramid is (input length):");
          double sideLength = myScanner.nextDouble();
          //This is what collects the value of the square side of the pyramid from the user
          System.out.print("The height of the pyramid is (input height):");
          double totalHeight = myScanner.nextDouble();
          //This is what collects the value from the user of the height of the pyramid
          double volumeInside = (sideLength * sideLength * totalHeight)/3;
          //This calculates the volume using a formula and the values from the user
          System.out.println("The volume inside the pyramid is " + volumeInside);
          //This prints the result of the calculation

}  //end of main method   
  	} //end of class
