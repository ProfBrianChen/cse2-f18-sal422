//Sarah Loher
//September 22, 2018
////This program will use only switch statements to do the following:
//It will ask the user if they want to input the dice combo or roll a random pair
//Once it takes the input from the user it will determine the slang term for the roll
//It will then print the slang term to the console
import java.util.Scanner;
public class CrapsSwitch{
    			// main method required for every Java program
   			public static void main(String[] args) {
        
          Scanner myScanner = new Scanner( System.in );
          System.out.println("Would you like to randomly roll the dice? (true or false)");
 
          boolean randomOrNot = myScanner.nextBoolean();
  
          int randomNumberOne = (int) (Math.random()* 6+1);
          int randomNumberTwo = (int) (Math.random()* 6+1);
          
          boolean snakeEyes = randomNumberOne==1 && randomNumberTwo==1;
          boolean aceDeuce = randomNumberOne==1 && randomNumberOne==2;
          boolean aceDeuceTwo = randomNumberOne==2 && randomNumberTwo==1;
          boolean easyFour = randomNumberOne==1 && randomNumberTwo==3;
          boolean easyFourTwo = randomNumberOne==3 && randomNumberTwo==1;
          boolean feverFive = randomNumberOne==1 && randomNumberTwo==4;
          boolean feverFiveTwo = randomNumberOne==4 && randomNumberTwo==1;
          boolean easySix = randomNumberOne==1 && randomNumberTwo==5;
          boolean easySixTwo = randomNumberOne==5 && randomNumberTwo==1;
          boolean sevenOut = randomNumberOne==1 && randomNumberTwo==6;
          boolean sevenOutTwo = randomNumberOne==6 && randomNumberTwo==1;
          boolean hardFour = randomNumberOne==2 && randomNumberTwo==2;
          boolean feverFiveThree = randomNumberOne==2 && randomNumberTwo==3;
          boolean feverFiveFour = randomNumberOne==3 && randomNumberTwo==2;
          boolean easySixThree = randomNumberOne==2 && randomNumberTwo==4;
          boolean easySixFour = randomNumberOne==4 && randomNumberTwo==2;
          boolean sevenOutThree = randomNumberOne==2 && randomNumberTwo==5;
          boolean sevenOutFour = randomNumberOne==5 && randomNumberTwo==2;
          boolean easyEight = randomNumberOne==2 && randomNumberTwo==6;
          boolean easyEightTwo = randomNumberOne==6 && randomNumberTwo==2;
          boolean hardSix = randomNumberOne==3 && randomNumberTwo==3;
          boolean sevenOutFive = randomNumberOne==3 && randomNumberTwo==4;
          boolean sevenOutSix = randomNumberOne==4 && randomNumberTwo==3;
          boolean easyEightThree = randomNumberOne==3 && randomNumberTwo==5;
          boolean easyEightFour = randomNumberOne==5 && randomNumberTwo==3;
          boolean nine = randomNumberOne==3 && randomNumberTwo==6;
          boolean nineTwo = randomNumberOne==6 && randomNumberTwo==3;
          boolean hardEight = randomNumberOne==4 && randomNumberTwo==4;
          boolean nineThree = randomNumberOne==4 && randomNumberTwo==5;
          boolean nineFour = randomNumberOne==5 && randomNumberTwo==4;
          boolean easyTen = randomNumberOne==4 && randomNumberTwo==6;
          boolean easyTenTwo = randomNumberOne==6 && randomNumberTwo==4;
          boolean hardTen = randomNumberOne==5 && randomNumberTwo==5;
          boolean yoLeven = randomNumberOne==5 && randomNumberTwo==6;
          boolean yoLevenTwo = randomNumberOne==6 && randomNumberTwo==5;
          boolean boxCars = randomNumberOne==6 && randomNumberTwo==6;
          
          switch(snakeEyes){
            case true:
                System.out.println("You rolled snake eyes");
              break;
          }
          switch(aceDeuce){
            case true:
             System.out.println("You rolled an ace deuce");
              break;
          }
          switch(aceDeuceTwo){
            case true:
              System.out.println("You rolled an ace deuce");
              break;
          }
          switch(easyFour){
            case true:
                System.out.println("You rolled an easy four");
              break;
          }
            switch(easyFourTwo){
              case true:
                System.out.println("You rolled an easy four");
              break;
            }
            switch(feverFive){
              case true:
                System.out.println("You rolled a fever five");
              break;
            }
            switch(feverFiveTwo){
              case true: 
                System.out.println("You rolled a fever five");
              break;
        }
            switch(feverFiveThree){
                case true:
                System.out.println("You rolled a fever five");
              break;
            }
            switch(feverFiveFour){
                case true:
                System.out.println("You rolled a fever five");
              break;
            }
            switch(easySix){
                case true:
                System.out.println("You rolled an easy six");
              break;
            }
            switch(easySixTwo){
                case true:
                System.out.println("You rolled an easy six");
              break;
            }
            switch(easySixThree){
                case true:
                System.out.println("You rolled an easy six");
              break;
            }
            switch(easySixFour){
                case true:
                System.out.println("You rolled an easy six");
              break;
            }
            switch(sevenOut){
                case true:
                System.out.println("You rolled a seven out");
              break;
            }
            switch(sevenOutTwo){
                case true:
                System.out.println("You rolled a seven out");
              break;
            }
            switch(sevenOutThree){
                case true:
                System.out.println("You rolled a seven out");
              break;
            }
            switch(sevenOutFour){
                case true:
                System.out.println("You rolled a seven out");
              break;
            }
            switch(sevenOutFive){
                case true:
                System.out.println("You rolled a seven out");
              break;
            }
            switch(sevenOutSix){
                case true:
                System.out.println("You rolled a seven out");
              break;
            }
            switch(hardFour){
                case true:
                System.out.println("You rolled a hard four");
              break;
            }
            switch(easyEight){
                case true:
                System.out.println("You rolled an easy eight");
              break;
            }
            switch(easyEightTwo){
                case true:
                System.out.println("You rolled an easy eight");
              break;
            }
            switch(easyEightThree){
                case true:
                System.out.println("You rolled an easy eight");
              break;
            }
            switch(easyEightFour){
                case true:
                System.out.println("You rolled an easy eight");
              break;
            }
            switch(hardSix){
                case true:
                System.out.println("You rolled a hard six");
              break;
            }
            switch(nine){
                case true:
                System.out.println("You rolled a nine");
              break;
            }
            switch(nineTwo){
                case true:
                System.out.println("You rolled a nine");
              break;
            }
            switch(nineThree){
                case true:
                System.out.println("You rolled a nine");
              break;
            }
            switch(nineFour){
                case true:
                System.out.println("You rolled a nine");
              break;
            }
            switch(hardEight){
                case true:
                System.out.println("You rolled a hard eight");
              break;
            }
            switch(easyTen){
                case true:
                System.out.println("You rolled an easy ten");
              break;
            }
            switch(easyTenTwo){
                case true:
                System.out.println("You rolled an easy ten");
              break;
            }
            switch(hardTen){
                case true:
                System.out.println("You rolled a hard ten");
              break;
            }
            switch(yoLeven){
                case true:
                System.out.println("You rolled a yo-leven");
              break;
            }
            switch(yoLevenTwo){
                case true:
                System.out.println("You rolled a yo-leven");
              break;
            }
            switch(boxCars){
                case true:
                System.out.println("You rolled boxcars");
              break;
          }
          
          }
        }
//This program does not compile because I cannot figure out how to use the switch statements with the boolean
