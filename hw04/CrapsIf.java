//Sarah Loher
//September 22, 2018
//This program will use only if statements to do the following:
//It will ask the user if they want to input the dice combo or roll a random pair
//Once it takes the input from the user it will determine the slang term for the roll
//It will then print the slang term to the console

import java.util.Scanner;
public class CrapsIf{
    			// main method required for every Java program
   			public static void main(String[] args) {
        
          Scanner myScanner = new Scanner( System.in );
          System.out.println("Would you like to randomly roll the dice? (true or false)");
          //This will ask the user whether or not they'd like to roll the dice randomly or input numbers
 
          boolean randomOrNot = myScanner.nextBoolean();
          //This will return as true and run the first if statement if the person types true
          
            int randomNumberOne = (int) (Math.random()* 6+1);
            int randomNumberTwo = (int) (Math.random()* 6+1);
          //These will find two random numbers
         
          if (randomOrNot){
            //This block will run if they want it to be random
            if (randomNumberOne == 1 && randomNumberTwo == 1){
              System.out.println("You rolled snake eyes");
            }
            if (randomNumberOne == 1 && randomNumberTwo == 2){
              System.out.println("You rolled an Ace Deuce");
            }
            if (randomNumberOne == 2 && randomNumberTwo == 1){
              System.out.println("You rolled an Ace Deuce");
            }
            if (randomNumberOne == 1  && randomNumberTwo == 3){
              System.out.println("You rolled an Easy four");
            }
            if(randomNumberOne == 3 && randomNumberTwo == 1){
              System.out.println("You rolled an Easy Four");
            }
            if (randomNumberOne == 1  && randomNumberTwo == 4 ){
              System.out.println("You rolled a Fever Five");
            }
            if (randomNumberOne == 4 && randomNumberTwo==1){
              System.out.println("You rolled a Fever five");
            }
            if (randomNumberOne == 1  && randomNumberTwo == 5 ){
              System.out.println("You rolled an Easy Six");
            }
            if (randomNumberOne == 5 && randomNumberTwo == 1){
              System.out.println("You rolled an Easy Six");
            }
            if (randomNumberOne == 1 && randomNumberTwo == 6){
              System.out.println("You rolled a Seven out");
            }
            if (randomNumberOne==6 && randomNumberTwo==1){
              System.out.println("You rolled a Seven Out");
            }
            if (randomNumberOne == 2 && randomNumberTwo == 2){
              System.out.println("You rolled a Hard Four");
            }
            if (randomNumberOne == 2 && randomNumberTwo == 3){
              System.out.println("You rolled a Fever Five");
            }
            if (randomNumberOne==3 && randomNumberTwo==2){
              System.out.println("You rolled a Fever Five");
            }
            if (randomNumberOne == 2 && randomNumberTwo == 4){
              System.out.println("You rolled an Easy Six");
            }
            if (randomNumberOne==4 && randomNumberTwo==2){
              System.out.println("You rolled an Easy Six");
            }
            if (randomNumberOne == 2 && randomNumberTwo == 5){
              System.out.println("You rolled a Seven Out");
            }
            if (randomNumberOne==5 && randomNumberTwo==2){
              System.out.println("You rolled a Seven Out");
            }
            if (randomNumberOne == 2 && randomNumberTwo == 6){
              System.out.println("You rolled an Easy Eight");
            }
            if (randomNumberOne==6 && randomNumberTwo==2){
              System.out.println("You rolled an Easy eight");
            }
            if (randomNumberOne == 3 && randomNumberTwo == 3){
              System.out.println("You rolled a Hard Six");
            }
            if (randomNumberOne == 3 && randomNumberTwo == 4){
              System.out.println("You rolled a Seven Out");
            }
            if (randomNumberOne==4 && randomNumberTwo==3){
              System.out.println("You rolled a seven out");
            }
            if (randomNumberOne == 3 && randomNumberTwo == 5){
              System.out.println("You rolled an Easy Eight");
            }
            if (randomNumberOne== 5 && randomNumberTwo==3){
              System.out.println("You rolled an Easy Eight");
            }
            if (randomNumberOne == 3 && randomNumberTwo == 6){
              System.out.println("You rolled a Nine");
            }
            if (randomNumberOne==6 && randomNumberTwo==3){
              System.out.println("You rolled a nine");
            }
            if (randomNumberOne == 4 && randomNumberTwo == 4){
              System.out.println("You rolled a Hard Eight");
            }
            if (randomNumberOne == 4 && randomNumberTwo == 5){
              System.out.println("You rolled a Nine");
            }
            if(randomNumberOne==5 && randomNumberTwo==4){
              System.out.println("You rolled a Nine");
            }
            if (randomNumberOne == 4 && randomNumberTwo == 6){
              System.out.println("You rolled an Easy Ten");
            }
            if (randomNumberOne==6 && randomNumberTwo==4){
              System.out.println("You rolled an Easy Ten");
            }
            if (randomNumberOne == 5 && randomNumberTwo == 5){
              System.out.println("You rolled a Hard Ten");
            }
            if (randomNumberOne == 5 && randomNumberTwo == 6){
              System.out.println("You rolled a Yo-leven");
            }
            if (randomNumberOne== 6 && randomNumberTwo==5){
              System.out.println("You rolled a Yo-leven");
            }
            if (randomNumberOne==6 && randomNumberTwo==6){
              System.out.println("You rolled Boxcars");
            }
           
          }
          else {
            //This will run if they do not want it to be random
          System.out.print("Enter a value from 1-6 for dice one ");
            int diceOne = myScanner.nextInt();
            //This will allow them to put in a first value
          System.out.print("Enter a value from 1-6 for dice two ");
            int diceTwo = myScanner.nextInt();
            //This will allow them to put a second value
            if (diceOne == 1 && diceTwo == 1){
              System.out.println("You rolled snake eyes");
            }
            if (diceOne == 1 && diceTwo == 2){
              System.out.println("You rolled an Ace Deuce");
            }
            if (diceOne == 2 && diceTwo == 1){
              System.out.println("You rolled an Ace Deuce");
            }
            if (diceOne == 1  && diceTwo == 3){
              System.out.println("You rolled an Easy four");
            }
            if(diceOne == 3 && diceTwo == 1){
              System.out.println("You rolled an Easy Four");
            }
            if (diceOne == 1  && diceTwo == 4 ){
              System.out.println("You rolled a Fever Five");
            }
            if (diceOne == 4 && diceTwo==1){
              System.out.println("You rolled a Fever five");
            }
            if (diceOne == 1  && diceTwo == 5 ){
              System.out.println("You rolled an Easy Six");
            }
            if (diceOne == 5 && diceTwo == 1){
              System.out.println("You rolled an Easy Six");
            }
            if (diceOne == 1 && diceTwo == 6){
              System.out.println("You rolled a Seven out");
            }
            if (diceOne==6 && diceTwo==1){
              System.out.println("You rolled a Seven Out");
            }
            if (diceOne == 2 && diceTwo == 2){
              System.out.println("You rolled a Hard Four");
            }
            if (diceOne == 2 && diceTwo == 3){
              System.out.println("You rolled a Fever Five");
            }
            if (diceOne==3 && diceTwo==2){
              System.out.println("You rolled a Fever Five");
            }
            if (diceOne == 2 && diceTwo == 4){
              System.out.println("You rolled an Easy Six");
            }
            if (diceOne==4 && diceTwo==2){
              System.out.println("You rolled an Easy Six");
            }
            if (diceOne == 2 && diceTwo == 5){
              System.out.println("You rolled a Seven Out");
            }
            if (diceOne==5 && diceTwo==2){
              System.out.println("You rolled a Seven Out");
            }
            if (diceOne == 2 && diceTwo == 6){
              System.out.println("You rolled an Easy Eight");
            }
            if (diceOne==6 && diceTwo==2){
              System.out.println("You rolled an Easy eight");
            }
            if (diceOne == 3 && diceTwo == 3){
              System.out.println("You rolled a Hard Six");
            }
            if (diceOne == 3 && diceTwo == 4){
              System.out.println("You rolled a Seven Out");
            }
            if (diceOne==4 && diceTwo==3){
              System.out.println("You rolled a seven out");
            }
            if (diceOne == 3 && diceTwo == 5){
              System.out.println("You rolled an Easy Eight");
            }
            if (diceOne== 5 && diceTwo==3){
              System.out.println("You rolled an Easy Eight");
            }
            if (diceOne == 3 && diceTwo == 6){
              System.out.println("You rolled a Nine");
            }
            if (diceOne==6 && diceTwo==3){
              System.out.println("You rolled a nine");
            }
            if (diceOne == 4 && diceTwo == 4){
              System.out.println("You rolled a Hard Eight");
            }
            if (diceOne == 4 && diceTwo == 5){
              System.out.println("You rolled a Nine");
            }
            if(diceOne==5 && diceTwo==4){
              System.out.println("You rolled a Nine");
            }
            if (diceOne == 4 && diceTwo == 6){
              System.out.println("You rolled an Easy Ten");
            }
            if (diceOne==6 && diceTwo==4){
              System.out.println("You rolled an Easy Ten");
            }
            if (diceOne == 5 && diceTwo == 5){
              System.out.println("You rolled a Hard Ten");
            }
            if (diceOne == 5 && diceTwo == 6){
              System.out.println("You rolled a Yo-leven");
            }
            if (diceOne== 6 && diceTwo==5){
              System.out.println("You rolled a Yo-leven");
            }
            if (diceOne==6 && diceTwo==6){
              System.out.println("You rolled Boxcars");
            }
          }
        }
}
       