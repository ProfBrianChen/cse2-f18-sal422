//Sarah Loher
//September 8, 2018
//CSE002
// This program will calculate the total cost of buying pants, shirt and belts
//It will also find the sales tax on each type of clothing
//Then it will find the total cost with and without sales tax included
//This program will include a function that truncates the decimals to make each number look like a price
public class Arithmetic{
  
  public static void main(String args[]){
    
int numPants = 3; //Number of pairs of pants
double pantsPrice = 34.98; //Cost per pair of pants

int numShirts = 2; //Number of sweatshirts
double shirtPrice = 24.99; //Cost per shirt

int numBelts = 1; //Number of belts
double beltPrice = 33.99; //Cost per belt

double paSalesTax = 0.06; //The tax rate in Pennsylvania

double totalCostOfPants = numPants*pantsPrice; //This will find the total cost of all the pants
double totalCostOfShirts = numShirts*shirtPrice; //This will find the total cost of all the shirts
double totalCostOfBelts = numBelts*beltPrice; //This will find the total cost of all the belts

double pantsSalesTax = totalCostOfPants*paSalesTax; //This will find the sales tax on the pants
    double pantsRoundOff = Math.floor(pantsSalesTax*100)/100.0; //This function will truncate the number to only 2 places after the decimal
double shirtsSalesTax = totalCostOfShirts*paSalesTax;//This will find the sales tax on the shirts
    double shirtsRoundOff = Math.floor(shirtsSalesTax*100)/100.0;//This will truncate the number to 2 places after the decimal
double beltsSalesTax = totalCostOfBelts*paSalesTax;//This will find the sales tax on the belts
    double beltsRoundOff = Math.floor(beltsSalesTax*100)/100.0;//This will truncate the decimal

double totalCostOfPurchaseNoTax = pantsPrice+shirtPrice+beltPrice;
    //This will find the total cost of the purchae 
    double totalNoTax = Math.floor(totalCostOfPurchaseNoTax*100)/100.0;
    //This truncates the decimal value that is found

double salesTaxTotal = pantsSalesTax+shirtsSalesTax+beltsSalesTax;
    //This finds the total sales tax by adding the sales tax on each type of item
    double salesTaxRoundOff = Math.floor(salesTaxTotal*100)/100.0;
    //This will truncate the decimal

double totalCostOfPurchaseWithTax = totalCostOfPurchaseNoTax+salesTaxTotal;
    //This will add the sales tax to the total purchase to find the total of the purchase WITH tax
    double totalWithTax = Math.floor(totalCostOfPurchaseWithTax*100)/100.0;
    //This will truncate the decimal

    System.out.println(  "The total cost of pants is " + totalCostOfPants);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The total cost of shirts is " + totalCostOfShirts);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The total cost of belts is " + totalCostOfBelts);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The sales tax for the pants is " + pantsRoundOff);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The sales tax for the shirts is " + shirtsRoundOff);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The sales tax for the belts is " + beltsRoundOff);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The total sales tax for the purchase is " + salesTaxRoundOff);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The total cost of the purchase before tax is " + totalNoTax);
    //This prints the total cost of only the pants with a phrase
    System.out.println("The total cost of the purchase including tax is " + totalWithTax);
    //This prints the total cost of only the pants with a phrase
  
  }
}


