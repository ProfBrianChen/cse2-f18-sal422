//Sarah Loher
//October 30, 2018
//This program will ask a user for input and give a menu of options and based on which option the user enters
//it will run a different method and output the desired result

import java.util.Scanner;

public class WordTools{
  public static void main(String args[]){//This is the main method
    String sample = sampleText();//This is the string that is what text the user entered
    String word = "";//This is the second string argument required for the statement below
    System.out.println(printMenu(sample, word ));//This prints the menu and whatever comes from commands given to the menu method
    }
  public static String sampleText(){//This is the method that stores the sample text
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter a sample text as a string...");
    String sample = sc.nextLine();
    return sample;//This returns the sample text to be able to be used later
  }
  public static String printMenu(String sample, String find){//This is the print menu method which also prints other methods based on the option the user picks
    System.out.println("This is the menu of options for analysis of the string:");
    System.out.println("c - number of non-white space characters");//If the user chooses this option the getNumOfNonWSCharacters method will run
    System.out.println("w - number of words");//If the user chooses this option the getNumOfWords method will run
    System.out.println("f - find text");//If the user chooses this option the findText method will run
    System.out.println("r - replace all !'s");//If the user chooses this option the replaceExclamation method will run
    System.out.println("s - shorten spaces");//If the user chooses this option the shortenSpace method will run
    System.out.println("q - quit");//This option will not do anything 
    System.out.println("Choose an option by entering the corresponding character: ");
    Scanner menuSc = new Scanner(System.in);//This is a new scanner created to 
    String output = menuSc.nextLine();
   while(true){
    if(output.equals("c")){//This determines whether the user enters c or not
    return "Number of non-white characters: " +  getNumOfNonWSCharacters(sample);//this prints if the user enters c and the if statement is started
   }
   else if(output.equals("w")){///This determines whether the user enters w or not
    return "you had "  + getNumOfWords(sample)+ " words";//this prints if the user enters w and the if statement is started
   }
   else if(output.equals("f")){//This determines whether the user enters f or not
     String word = "";
     return "Your word appeared " + findText(word, sample) + " times.";//this prints if the user enters f and the if statement is started
   }
   else if(output.equals("r")){//This determines whether the user enters r or not
    return "Your new string looks like: " +replaceExclamation(sample);//this prints if the user enters r and the if statement is started
   }
   else if(output.equals("s")){//This determines whether the user enters s or not
     return "Your new string looks like: " + shortenSpace(sample);//this prints if the user enters s and the if statement is started
   }
   else if(output.equals("q")){//This determines whether the user enters q or not
     return "you picked to quit";//this prints if the user enters q and the if statement is started..this stops the program
   }
   else{
     System.out.println("You entered an invalid character");//This tells the user that they entered something that isnt an option
     return printMenu(sample, find);//This runs again and asks for a new option
     
   }
  }
}
  public static int getNumOfNonWSCharacters(String sample){//This method finds the number of characters that arent spaces
    //This method is called when the user inputs option c
    String newString = sample.replaceAll("\\s+", "");//This deletes the spaces between the words
    int length = newString.length();//This takes the length of the new string without the spaces
    return length;//This returns the length of the new string
  }
  public static int getNumOfWords(String sample){//This method finds the number or words
    String[] words = sample.split("\\s+");//this splits the words
    return words.length;//this returns the amount of words
  }
  public static int findText(String word, String sample){//This method will find a specific word in a string
    String finder = ""; int count = 0;//This creates 2 variables
    
      Scanner input = new Scanner(System.in);//This creates a new scanner to hold the word that is supposed to be found
      System.out.println("Enter a word to be found in your input: ");
      finder = input.next();     //This stores the word that the user wants to find
   
      String a[] = sample.split(" "); 
        for (int i = 0; i < a.length; i++) {//This starts the counter
            if(a[i].equals(finder)){
                count++;//This increments the counter
            }
    }   
    return count;  //This returns the amount of times the word appeared
  }
  public static String replaceExclamation(String sample){ //This method replaces everything with exclamation marks 
    int len = sample.length();//This finds the length of the sample text
    StringBuilder sb = new StringBuilder(len);
    for(int i = 0; i < len; i++){
      sb.append('!');//This replaces the characters with !
    }
  return sb.toString();//This returns the new string
}
  public static String shortenSpace(String sample){//This method shortens spaces to 1 when the user inputs more than 1 in a row
    String shortenedSpaces = sample.replaceAll("\\s{2,}", " ");//This finds times when there are 2 or more spaces and changes it to 1 space
    return shortenedSpaces;//This returns the new string with shortened spaces
  }
}