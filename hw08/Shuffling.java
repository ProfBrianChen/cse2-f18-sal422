//Sarah Loher
//Tuesday November 13
//This will print a deck of cards, shuffle the cards and then print a hand of cards

import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;

public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
String[] suitNames={"C","H","S","D"};   //These represent the suits clubs, hearts, spades and diamonds respectively 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //These represent the number values and symbols to represent the face cards
String[] cards = new String[52]; //This creates 52 numbers
String[] hand = new String[5]; //This creates a hand of 5 cards
int numCards = 0; 
int again = 1; //This is a temporary variable that will change based on the users input of whether they want another hand
int index = 51;
printArray(cards); 
shuffle(cards); 
while(again == 1){ //This starts the while-loop that runs until the user doesnt enter 0
   getHand(cards,index,numCards);//This prints the value from the getHand method
   index = index-numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); //This prints a statement asking the user if they want another hand
   again = scan.nextInt(); //This collects the value the user enters
}  
  } //This closes the main method
  
public static void printArray(String[] cards){//This method will print out the whole deck of cards
  String[] suitNames={"C","H","S","D"};   //These represent the suits clubs, hearts, spades and diamonds respectively 
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //These represent the number values and symbols to represent the face cards
  String[] hand = new String[5]; //This creates a hand of 5 cards
  int numCards = 0; 
  int again = 1; //This is a temporary variable that will change based on the users input of whether they want another hand
  int index = 51;
  for (int i=0; i<52; i++){ //This for loop creates the number and letter pairs for the cards
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); //THis prints the cards seperated by a space
  } 
  System.out.println();//This prints a new line
 }
public static void shuffle(String [] cards){//This method will shuffle the deck of cards printed in the method printArray 
  String[] randomString = new String[52];//This creates a new string with enough space for the entire deck
  String printingRandom = "";//This is an empty string to hold what gets printed
  Random rnd = ThreadLocalRandom.current();//This creates a random thread
  for (int l = cards.length - 1; l > 0; l--){
    int index = rnd.nextInt(l + 1);//This changes index
    String CardA = cards[index];//This creates a string called CardA
    cards[index] = cards[l];//this sets the two arrays equal
    cards[l] = CardA;
    randomString = cards;//this makes random string equal to the original string
    printingRandom = Arrays.toString(randomString);//This changes the array to a printable string
  }
  System.out.println(printingRandom);//This prints the shuffled list
  System.out.println();
}
public static void getHand(String[] cards, int index, int numCards){//This creates a method that gets a hand of 5 cards
  String[] hand = new String[5];//This creates the string hand which has room for 5 cars
  String[] randomString = cards;//This sets randomString to cards
  randomString = cards;//This does the same thing
  for(int m=0; m<5; m++){//This finds the first 5 cards
   System.out.print(randomString[m] + ", ");//This prints the hand
  }
  System.out.println();
}
}

