//Sarah Loher
//October 11, 2018
//This program will show pattern C using for loops

import java.util.Scanner;

public class PatternC{
  public static void main(String[] args){
    
    Scanner sc = new Scanner(System.in);//This calls the scanner class
        
        System.out.println("How many rows you want in this pattern?");//This asks the user how many rows
    
    int rows = sc.nextInt();//This stores the value that the user puts in
    
    String space = " ";
     for (int i = 1; i <= rows+1; i++){ //This sets the conditions for the loop to run
       for (int j = 1; j < i; j++){
                System.out.print(" ");
            }
            for (int j = i; j >= 1; j--){//This prints the values for the pyramid
                System.out.print(j);
            }
             
            System.out.println();
        }
        sc.close();
  }
}