//Sarah Loher
//October 11, 2018
//This will print out the pattern in pyramid B

import java.util.Scanner;
 
public class PatternB{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);//This calls the scanner class
        
        System.out.println("How many rows you want in this pattern?");//This asks the user how many rows
         
        int rows = sc.nextInt();//This stores the value that the user enters
         
        for (int i = rows; i >= 1; i--){//This sets i equal to the user input, makes sure it is more than one and decreases i each time
            for (int j = 1; j <= i; j++){//THis sets j equal to 1, makes sure j is less than or equal to i, then adds to j each time
                System.out.print(j+" ");
            }
             
            System.out.println();
        }
         
        sc.close();
    }
}