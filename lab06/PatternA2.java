//Sarah Loher
//October 11, 2018
//This file will push pattern A through using nested loops

import java.util.Scanner;
 
public class PatternA{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);//This calls the scanner class which asks the user how many rows
         
        System.out.println("How many rows you want in this pattern?");//This is the statement that is printed 
         
        int rows = sc.nextInt();//This stores the value that the user puts in
         
        for (int i = 1; i <= rows; i++){ //This sets the conditions for the loop to run
            for (int j = 1; j <= i; j++){//This prints the values for the pyramid
                System.out.print(j+" ");
            }
             
            System.out.println();
        }
        sc.close();
    }
}