//Sarah Loher
//October 11, 2018
//This will print pattern D using for loops

import java.util.Scanner;

public class PatternD{
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);//This calls the scanner class which asks the user how many rows
    
     System.out.println("How many rows you want in this pattern?");//This asks the user how many rows
         
        int rows = sc.nextInt();//This stores the value that the user enters
        
        for (int i = rows; i >= 1; i--){//This sets i equal to the user input, makes sure it is more than one and decreases i each time
            for (int j = i; j >= 1; j--){
                System.out.print(j+" ");//This prints the number with a space
              
            }
             
            System.out.println();
        }
         
        sc.close();
  }
}